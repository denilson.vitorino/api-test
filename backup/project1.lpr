program project1;

{$MODE DELPHI}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Horse,
  Horse.Jhonson, Controller.Boleto,
  fpjson,
  SysUtils;

begin
  Application.Title := 'My Application';
  THorse.Use(Jhonson);

  Controller.Boleto.registry;

  THorse.Listen(8000);
end.
