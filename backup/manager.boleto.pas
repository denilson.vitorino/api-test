unit Manager.Boleto;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, Model.Boleto, DateUtils;

type

  { TBoletoManager }

  TBoletoManager = class
    private
      procedure alimentarDados;

    public
      procedure limparListaBoletos;
      procedure gerarBoleto(boleto: TBoleto);

  end;

implementation

//uses uDtmACBr;

{ TBoletoManager }

procedure TBoletoManager.alimentarDados;
begin
  //with dmACBr.ACBrBoleto1 do
  //begin
  //
  //  ACBrBoletoFC            := dmAcbr.ACBrBoletoFCFortes1;
  //  Homologacao             := false;
  //  ImprimirMensagemPadrao  := true;
  //  LeCedenteRetorno        := true;
  //  LerNossoNumeroCompleto  := false;
  //  Cedente.Modalidade      := '1';
  //  LayoutRemessa           := c400;
  //  DirArqRemessa           := '';
  //  DirArqRetorno           := '';
  //  NomeArqRemessa          := '';
  //  NomeArqRetorno          := '';
  //  NumeroArquivo           := 0;
  //  RemoveAcentosArqRemessa := true;
  //
  //  with Banco do
  //  begin
  //    TipoCobranca           := cobBancoInter;
  //    CasasDecimaisMoraJuros := 2;
  //    Numero                 := 71;
  //    //LayoutVersaoArquivo := 107;
  //    //LayoutVersaoLote := 67;
  //  end;
  //
  //  with Cedente do
  //  begin
  //    Agencia            := '0000';
  //    AgenciaDigito      := '9';
  //    CaracTitulo        := tcSimples;
  //    CodigoCedente      := '000000';
  //    Conta              := '00000000';
  //    ContaDigito        := '0';
  //    IdentDistribuicao  := tbBancoDistribui;
  //    ResponEmissao      := tbCliEmite;
  //    TipoCarteira       := tctSimples;
  //    TipoDocumento      := Escritural;
  //    TipoInscricao      := pJuridica;
  //    Cedente.Bairro     := 'Teste';
  //    Cedente.CEP        := '95800000';
  //    Cedente.Cidade     := 'Teste';
  //    Cedente.CNPJCPF    := '00000000000000';
  //    Cedente.Logradouro := 'Teste';
  //    Cedente.Nome       := 'Teste';
  //    Cedente.NumeroRes  := '0';
  //    Cedente.Telefone   := '51999999999';
  //    Cedente.UF         := 'RS';
  //  end;
  //
  //end;

end;

procedure TBoletoManager.limparListaBoletos;
begin
  //dmAcbr.ACBrBoleto1.ListadeBoletos.Clear;
end;

procedure TBoletoManager.gerarBoleto(boleto: TBoleto);
//var
//   titulo : TACBrTitulo;
begin
  //titulo := dmAcbr.ACBrBoleto1.CriarTituloNaLista;
  //
  //with titulo do
  //begin
  //
  //  LocalPagamento       := 'PAGAVEL EM QUALQUER BANCO';
  //  Vencimento           := boleto.vcto;
  //  DataDocumento        := Da//titulo := dmAcbr.ACBrBoleto1.CriarTituloNaLista;
  //
  //with titulo do
  //begin
  //
  //  LocalPagamento       := 'PAGAVEL EM QUALQUER BANCO';
  //  Vencimento           := boleto.vcto;
  //  DataDocumento        := Date;
  //  NumeroDocumento      := '1';
  //  EspecieDoc           := 'DM';
  //  Aceite               := atSim;
  //  DataProcessamento    := Now;
  //  Carteira             := '112';
  //  NossoNumero          := '1';
  //  ValorDocumento       := boleto.valorDocumento;
  //  TipoImpressao        :=tipNormal;
  //  ValorDespesaCobranca := 0;
  //  DataLimitePagto      := IncDay(boleto.vcto,30);
  //  DiasDeProtesto       := 30;
  //  CodigoMulta          := cmPercentual;
  //  DataMulta            := IncDay(boleto.vcto,1);
  //  PercentualMulta      := boleto.percentualMulta;
  //  MultaValorFixo       := false;
  //  CodigoMora           := '1';
  //  CodigoMoraJuros      := cjValorDia;
  //  DataMoraJuros        := IncDay(boleto.vcto,1);
  //  ValorMoraJuros       := RoundTo(boleto.valorDocumento * (boleto.valorMoraJuros / 100),-2);
  //
  //  with Sacado do
  //  begin
  //    NomeSacado := 'Teste';
  //    CNPJCPF    := '000000000000000';
  //    Pessoa     := pJuridica;
  //    Logradouro := 'Teste';
  //    Numero     := '1';
  //    Bairro     := 'Teste';
  //    Cidade     := 'Teste';
  //    UF         := 'RS';
  //    CEP        := '95800000';
  //  end;
  //end;                               te;
  //  NumeroDocumento      := '1';
  //  EspecieDoc           := 'DM';
  //  Aceite               := atSim;
  //  DataProcessamento    := Now;
  //  Carteira             := '112';
  //  NossoNumero          := '1';
  //  ValorDocumento       := boleto.valorDocumento;
  //  TipoImpressao        :=tipNormal;
  //  ValorDespesaCobranca := 0;
  //  DataLimitePagto      := IncDay(boleto.vcto,30);
  //  DiasDeProtesto       := 30;
  //  CodigoMulta          := cmPercentual;
  //  DataMulta            := IncDay(boleto.vcto,1);
  //  PercentualMulta      := boleto.percentualMulta;
  //  MultaValorFixo       := false;
  //  CodigoMora           := '1';
  //  CodigoMoraJuros      := cjValorDia;
  //  DataMoraJuros        := IncDay(boleto.vcto,1);
  //  ValorMoraJuros       := RoundTo(boleto.valorDocumento * (boleto.valorMoraJuros / 100),-2);
  //
  //  with Sacado do
  //  begin
  //    NomeSacado := 'Teste';
  //    CNPJCPF    := '000000000000000';
  //    Pessoa     := pJuridica;
  //    Logradouro := 'Teste';
  //    Numero     := '1';
  //    Bairro     := 'Teste';
  //    Cidade     := 'Teste';
  //    UF         := 'RS';
  //    CEP        := '95800000';
  //  end;
  //end;
end;

end.

