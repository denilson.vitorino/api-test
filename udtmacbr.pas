unit uDtmACBr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ACBrBoleto, ACBrBoletoFCFortesFr;

type

  { TdmACBr }

  TdmACBr = class(TDataModule)
    ACBrBoleto1: TACBrBoleto;
    ACBrBoletoFCFortes1: TACBrBoletoFCFortes;
  private

  public

  end;

var
  dmACBr: TdmACBr;

implementation

{$R *.lfm}

end.

