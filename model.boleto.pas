unit Model.Boleto;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { TBoleto }

  TBoleto = class
    private
      FpercentualMulta: double;
      FvalorDocumento: double;
      FvalorMoraJuros: double;
      Fvcto: TDate;
      procedure SetpercentualMulta(AValue: double);
      procedure SetvalorDocumento(AValue: double);
      procedure SetvalorMoraJuros(AValue: double);
      procedure Setvcto(AValue: TDate);

    public
      property vcto: TDate read Fvcto write Setvcto;
      property valorDocumento: double read FvalorDocumento write SetvalorDocumento;
      property valorMoraJuros: double read FvalorMoraJuros write SetvalorMoraJuros;
      property percentualMulta: double read FpercentualMulta write SetpercentualMulta;
  end;


implementation

{ TBoleto }

procedure TBoleto.SetpercentualMulta(AValue: double);
begin
  if FpercentualMulta = AValue then Exit;
  FpercentualMulta := AValue;
end;

procedure TBoleto.SetvalorDocumento(AValue: double);
begin
  if FvalorDocumento = AValue then Exit;
  FvalorDocumento := AValue;
end;

procedure TBoleto.SetvalorMoraJuros(AValue: double);
begin
  if FvalorMoraJuros = AValue then Exit;
  FvalorMoraJuros := AValue;
end;

procedure TBoleto.Setvcto(AValue: TDate);
begin
  if Fvcto = AValue then Exit;
  Fvcto := AValue;
end;

end.

